<?php
//require_once siempre que importemos una clase
require_once('User.php');
//iniciar sesion
session_start();
if (!isset($_SESSION['users'])) {
    $_SESSION['users'] = array();
}
//tomar el usuario del request si es posible
//añadirlo a la sesion
if (!empty($_REQUEST['name'])) {
    $user = new User($_REQUEST);
    $_SESSION['users'][] = $user;
}
//reenviar a "index.php"
header('Location: index.php');

