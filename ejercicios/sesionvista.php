<!DOCTYPE html>
<html>
<head>
    <title>Sesiones</title>
</head>
<body>

<section>
    <h1>Sesiones</h1>

    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
        <label>Nombre</label>
        <input type="text" name="nombre">
        <hr>
        <input type="submit" value="Nuevo">
    </form>

    <h1>Lista de jugadores</h1>
    <ul>
        <?php foreach ($_SESSION['lista'] as $jugador): ?>
            <li><?php echo $jugador ?></li>
        <?php endforeach ?>
    </ul>

    <h2>Var_dump de la session</h2>
    <pre>
        <?php var_dump($_SESSION) ?>
    </pre>
</section>
</body>
</html>
