<!DOCTYPE html>
<html><head>
        <meta charset="UTF-8">
        <title>ejemplo 6</title>
</head><body>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mvc17";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT id, nombre, apellido, email FROM users";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "id: " . $row["id"]. " - Name: " . $row["nombre"]. " " . $row["apellido"]. " " . " - Email de contacto: " . $row["email"] .  "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>
</body></html>
